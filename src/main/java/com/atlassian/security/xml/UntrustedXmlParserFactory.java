package com.atlassian.security.xml;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLInputFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING;
import static javax.xml.stream.XMLInputFactory.*;

/**
 * TODO: Document this class / interface here
 *
 * @since x.x
 */
public class UntrustedXmlParserFactory
{
    private static InputSource EMPTY_INPUT_SOURCE = new InputSource(new ByteArrayInputStream(new byte[0]));

    private static EntityResolver emptyEntityResolver = new EntityResolver()
    {
        public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException
        {
            return EMPTY_INPUT_SOURCE;
        }
    };

    public static DocumentBuilder newDocumentBuilder() throws ParserConfigurationException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        dbf.setNamespaceAware(false);

        // Only necessary for bundled non-JDK Xerces
        dbf.setFeature(FEATURE_SECURE_PROCESSING, true);

        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);

        dbf.setAttribute("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

        return dbf.newDocumentBuilder();
    }

    public static XMLReader newXmlReader() throws SAXException, ParserConfigurationException
    {
        SAXParserFactory spf = SAXParserFactory.newInstance();

        spf.setNamespaceAware(false);
        spf.setFeature(FEATURE_SECURE_PROCESSING, true);

        spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);


        XMLReader xmlReader = spf.newSAXParser().getXMLReader();

        xmlReader.setEntityResolver(emptyEntityResolver);

        return xmlReader;
    }

    public static XMLReader newNamespaceAwareXmlReader() throws SAXException, ParserConfigurationException
    {
        SAXParserFactory spf = SAXParserFactory.newInstance();

        spf.setNamespaceAware(true);
        spf.setFeature(FEATURE_SECURE_PROCESSING, true);

        spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

        XMLReader xmlReader = spf.newSAXParser().getXMLReader();

        xmlReader.setEntityResolver(emptyEntityResolver);

        return xmlReader;
    }

    public static DocumentBuilder newNamespaceAwareDocumentBuilder() throws ParserConfigurationException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        dbf.setNamespaceAware(true);

        // Only necessary for bundled non-JDK Xerces
        dbf.setFeature(FEATURE_SECURE_PROCESSING, true);

        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);

        dbf.setAttribute("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

        return dbf.newDocumentBuilder();
    }

    public static XMLInputFactory newXmlInputFactory()
    {
        XMLInputFactory fac = newFactory();
        fac.setProperty(SUPPORT_DTD, Boolean.FALSE);

        return fac;
    }
}
