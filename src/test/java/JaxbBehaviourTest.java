import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;

import com.atlassian.security.xml.UntrustedXmlParserFactory;
import org.junit.Test;
import org.xml.sax.InputSource;

import static com.atlassian.security.xml.UntrustedXmlParserFactory.newXmlReader;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class JaxbBehaviourTest
{
    @Test
    public void unmarshallingWithProvidedXmlReader() throws Exception
    {
        JAXBContext context = JAXBContext.newInstance(Simple.class);

        String s = "<!DOCTYPE root SYSTEM '/no-such-file'> <simple><value>defined-in-xml</value></simple>";

        Unmarshaller um = context.createUnmarshaller();

        Object o;

//        o = um.unmarshal(new StringReader(s));
//        o = um.unmarshal(new StreamSource(new StringReader(s)));

        Source source = new SAXSource(newXmlReader(), new InputSource(new StringReader(s)));
        o = um.unmarshal(source);

        assertNotNull(o);
        assertEquals("defined-in-xml", ((Simple) o).value);
    }
}

@XmlRootElement
class Simple
{
    @XmlElement
    String value = "test";

    @Override
    public String toString()
    {
        return "Simple(" + value + ")";
    }
}
