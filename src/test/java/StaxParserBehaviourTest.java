import java.io.StringReader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.atlassian.security.xml.UntrustedXmlParserFactory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class StaxParserBehaviourTest
{
//     XMLInputFactory newFactory() throws Exception;
//        return XMLInputFactory.newFactory();

    XMLStreamReader createReader(String input) throws Exception
    {
        XMLInputFactory fac = UntrustedXmlParserFactory.newXmlInputFactory();

        XMLStreamReader sr = fac.createXMLStreamReader(new StringReader(input));
        return sr;
    }
//    XMLStreamReader createReader(String input) throws Exception
//    {
//        XMLInputFactory fac = newFactory();
//        fac.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
////        fac.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, Boolean.FALSE);
//
//        XMLStreamReader sr = fac.createXMLStreamReader(new StringReader(input));
//        return sr;
//    }

    static String gatherCharacters(XMLStreamReader sr) throws XMLStreamException
    {
        StringBuilder sb = new StringBuilder();

        while (sr.next() != XMLStreamConstants.END_DOCUMENT)
        {
            if (sr.getEventType() == XMLStreamConstants.CHARACTERS)
            {
                if (sr.hasText())
                {
                    sb.append(sr.getText());
                }
            }
        }

        return sb.toString();
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        XMLStreamReader sr = createReader(SampleXmlDocuments.AMPERSAND_DOCUMENT);

        assertEquals("&", gatherCharacters(sr));
    }

    @Test(expected = XMLStreamException.class, timeout = 1000)
    public void parseBillionLaughsDoesNotExhaustTime() throws Exception
    {
        XMLStreamReader sr = createReader(SampleXmlDocuments.BILLION_LAUGHS);

        while (sr.next() != XMLStreamConstants.END_DOCUMENT);
    }

    @Test(expected = XMLStreamException.class)
    public void externalEntityIsNotRead() throws Exception
    {
        XMLStreamReader sr = createReader(SampleXmlDocuments.externalResourceEntity());

        String text = gatherCharacters(sr);

//        System.out.println(text);
    }

    @Test
    public void dtdUriPointsToFile() throws Exception
    {
        XMLStreamReader sr = createReader(SampleXmlDocuments.EXTERNAL_DTD);

        assertEquals("", gatherCharacters(sr));
    }

    @Test
    public void dtdUriPointsToUrl() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        String s = SampleXmlDocuments.externalUrlDtd(detector.getUrl());

        XMLStreamReader sr = createReader(s);

        assertEquals("", gatherCharacters(sr));

        assertFalse("I don't want to see HTTP connection attempts", detector.wasAttempted());
    }
}
