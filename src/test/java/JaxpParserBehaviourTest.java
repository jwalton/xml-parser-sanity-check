import com.atlassian.security.xml.UntrustedXmlParserFactory;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class JaxpParserBehaviourTest
{
//    static DocumentBuilder createDocumentBuilder() throws ParserConfigurationException
//    {
////        System.setProperty("entityExpansionLimit", "0");
//
//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//
////        dbf.setExpandEntityReferences(false);
//
//        // This will turn any attempt to use a DTD into failure
////        dbf.setAttribute("http://apache.org/xml/features/disallow-doctype-decl", true);
//
//        // Only necessary for bundled non-JDK Xerces
//        dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
//
//        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
//        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
//
//        dbf.setAttribute("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
//
//        DocumentBuilder db = dbf.newDocumentBuilder();
////        db.setEntityResolver(new EntityResolver()
////        {
////            @Override
////            public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException
////            {
////                throw new IOException();
////            }
////        });
//        return db;
//    }

    static DocumentBuilder createDocumentBuilder() throws ParserConfigurationException
    {
        return UntrustedXmlParserFactory.newDocumentBuilder();
    }

    @Test(expected = IllegalArgumentException.class)
    public void askingForUnknownAttributesFails()
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setAttribute("no-attribute-with-this-identifier", false);
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        Document d = createDocumentBuilder().parse(new InputSource(new StringReader(SampleXmlDocuments.AMPERSAND_DOCUMENT)));
        Node n = d.getDocumentElement().getChildNodes().item(0);
        assertEquals("&", n.getTextContent());
    }

    @Test(expected = SAXParseException.class, timeout = 1000)
    public void parseBillionLaughsDoesNotExhaustMemory() throws Exception
    {
        createDocumentBuilder().parse(new InputSource(new StringReader(SampleXmlDocuments.BILLION_LAUGHS)));
    }

    @Test
    public void externalEntityIsNotIncludedInDom() throws Exception
    {
        Document d = createDocumentBuilder().parse(new InputSource(new StringReader(SampleXmlDocuments.externalResourceEntity())));
        assertEquals(0, d.getDocumentElement().getChildNodes().getLength());

        // If we didn't expandEntityReferences...
//        Node n = d.getDocumentElement().getChildNodes().item(0);
//
//        assertFalse(n instanceof Text);
//        assertTrue(n instanceof EntityReference);
    }

    @Test
    public void externalEntityIsNotRead() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        createDocumentBuilder().parse(new InputSource(
                new StringReader(SampleXmlDocuments.externalResourceEntity(detector.getUrl()))));

        assertFalse(detector.wasAttempted());
    }

    @Test
    public void externalParameterEntityIsNotRead() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        try
        {
            createDocumentBuilder().parse(new InputSource(
                    new StringReader(SampleXmlDocuments.externalParameterEntity(detector.getUrl()))));
        }
        catch (SAXParseException spe)
        {
            // Don't care
        }
        catch (IOException e)
        {
            // Don't care
        }

        assertFalse(detector.wasAttempted());
    }

    @Test
    public void dtdUriPointsToFile() throws Exception
    {
        Document d = createDocumentBuilder().parse(new InputSource(new StringReader(SampleXmlDocuments.EXTERNAL_DTD)));
        assertEquals("root", d.getDocumentElement().getTagName());
        assertEquals(0, d.getDocumentElement().getChildNodes().getLength());
    }

    @Test
    public void dtdUriPointsToUrl() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        String s = SampleXmlDocuments.externalUrlDtd(detector.getUrl());

        Document d = createDocumentBuilder().parse(new InputSource(new StringReader(s)));
        assertEquals("root", d.getDocumentElement().getTagName());
        assertEquals(0, d.getDocumentElement().getChildNodes().getLength());

        assertFalse("I don't want to see HTTP connection attempts", detector.wasAttempted());
    }
}
