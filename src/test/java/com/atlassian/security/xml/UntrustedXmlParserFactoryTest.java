package com.atlassian.security.xml;

import org.junit.Test;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * TODO: Document this class / interface here
 *
 * @since x.x
 */
public class UntrustedXmlParserFactoryTest
{
    @Test
    public void testNamespaceAwareIsNotEnabledByDefault() throws Exception
    {
        DocumentBuilder documentBuilder = UntrustedXmlParserFactory.newDocumentBuilder();

        String xml = "<x xmlns='a:b'/>";

        Document parse = documentBuilder.parse(new ByteArrayInputStream(xml.getBytes()));

        assertNull(parse.getDocumentElement().getNamespaceURI());
    }

    @Test
    public void testNamespaceAwareIsEnabledWhenRequested() throws Exception
    {
        DocumentBuilder documentBuilder = UntrustedXmlParserFactory.newNamespaceAwareDocumentBuilder();

        String xml = "<x xmlns='a:b'/>";

        Document parse = documentBuilder.parse(new ByteArrayInputStream(xml.getBytes()));

        assertEquals("a:b", parse.getDocumentElement().getNamespaceURI());
    }
}
