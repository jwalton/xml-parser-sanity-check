import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.StringReader;


public class StaxWithWstxParserBehaviourTest extends StaxParserBehaviourTest
{
    @Override
    XMLStreamReader createReader(String input) throws Exception
    {
        XMLInputFactory xmlInputFactory = new com.ctc.wstx.stax.WstxInputFactory();
        xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);

        return xmlInputFactory.createXMLStreamReader(new StringReader(input));
    }
}
