import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;

import com.atlassian.security.xml.UntrustedXmlParserFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

import nu.xom.ParsingException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class Dom4jSaxReaderBehaviourTest
{
    /**
     * Create a dom4j SAXBuilder using a configured SAX {@link XMLReader}.
     */
    SAXReader newSaxReader() throws SAXException, ParserConfigurationException
    {
        SAXReader saxReader = new SAXReader(UntrustedXmlParserFactory.newXmlReader());
        return saxReader;
    }

    @Test
    public void testDom4jParserIsBroken() throws IOException, ParsingException, DocumentException
    {
        final InputStream in = this.getClass().getResourceAsStream("/evil.xml");
        assertNotNull("Could not load /evil.xml as stream.", in);


        SAXReader xmlReader = new SAXReader();
        org.dom4j.Document document = xmlReader.read(in);

        assertTrue("Did not load SYSTEM entity containing TOP SECRET", document.getRootElement().getText().contains("TOP SECRET"));
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        Document d = newSaxReader().read(new StringReader(SampleXmlDocuments.AMPERSAND_DOCUMENT));
        assertEquals("&", d.getRootElement().getText());
    }

    @Test
    public void externalEntityIsNotIncludedInResult() throws Exception
    {
        Document d = newSaxReader().read(new StringReader(SampleXmlDocuments.externalResourceEntity()));

        assertEquals("", d.getRootElement().getText());
    }

    @Test
    public void externalEntityIsNotRead() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        Document d = newSaxReader().read(
                new StringReader(SampleXmlDocuments.externalResourceEntity(detector.getUrl())));

        assertFalse(detector.wasAttempted());
    }

    @Test(timeout = 1000)
    public void testDom4jParserDoesNotEvaluateTooMuch() throws Exception
    {
        SAXReader saxReader = newSaxReader();

        try
        {
            saxReader.read(new StringReader(SampleXmlDocuments.BILLION_LAUGHS));
        }
        catch (DocumentException e)
        {
            assertTrue(e.getNestedException() instanceof SAXParseException);
        }
    }

    @Test
    public void testDom4jParserDoesNotReadExternalDtds() throws Exception
    {
        SAXReader xmlReader = newSaxReader();

        final HttpAttemptDetector httpAttemptDetector = new HttpAttemptDetector();
        new Thread(httpAttemptDetector).start();

        final String s = SampleXmlDocuments.externalUrlDtd(httpAttemptDetector.getUrl());
        Document document = xmlReader.read(new StringReader(s));
        assertEquals("root", document.getRootElement().getName());
        assertFalse("I don't want to see HTTP connection attempts", httpAttemptDetector.wasAttempted());
    }
}
