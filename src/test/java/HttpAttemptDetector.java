import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Listen on a socket, flag connection attempts.
 */
public class HttpAttemptDetector implements Runnable
{
    private final ServerSocket serverSocket;
    private final AtomicBoolean someoneConnected = new AtomicBoolean(false);

    public HttpAttemptDetector() throws IOException
    {
        serverSocket = new ServerSocket(0);
    }

    String getUrl()
    {
        return "http://localhost:" + serverSocket.getLocalPort();
    }

    public void run()
    {
        while (true)
        {
            try
            {
                Socket s = serverSocket.accept();
                someoneConnected.set(true);
                s.close();
            }
            catch (IOException e)
            {
            }
        }
    }

    boolean wasAttempted()
    {
        return someoneConnected.get();
    }
}
