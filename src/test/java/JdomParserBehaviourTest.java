import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;

import com.atlassian.security.xml.UntrustedXmlParserFactory;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.JDOMParseException;
import org.jdom.input.SAXBuilder;
import org.junit.Test;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class JdomParserBehaviourTest
{
    SAXBuilder createSaxBuilder()
    {
        return new SAXBuilder() {
            @Override
            protected XMLReader createParser() throws JDOMException
            {
                try
                {
                    return UntrustedXmlParserFactory.newNamespaceAwareXmlReader();
                }
                catch (SAXException e)
                {
                    throw new RuntimeException(e);
                }
                catch (ParserConfigurationException e)
                {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    @Test(expected = JDOMException.class)
    public void settingUnknownFeaturesCausesFailure() throws JDOMException, IOException
    {
        SAXBuilder sb = new SAXBuilder();
        sb.setFeature("unknown-feature", false);

        /* An exception is only thrown when we try to parse after requesting an unknown feature */
        sb.build(new StringReader("<x/>"));
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        Document d = createSaxBuilder().build(new StringReader(SampleXmlDocuments.AMPERSAND_DOCUMENT));
        assertEquals("&", d.getRootElement().getText());
    }

    @Test(expected = JDOMParseException.class, timeout=1000)
    public void parseBillionLaughsDoesNotExhaustMemory() throws Exception
    {
        createSaxBuilder().build(new StringReader(SampleXmlDocuments.BILLION_LAUGHS));
    }

    @Test
    public void externalEntityIsNotIncludedInResult() throws Exception
    {
        Document d = createSaxBuilder().build(new StringReader(SampleXmlDocuments.externalResourceEntity()));

        assertEquals("", d.getRootElement().getText());
    }

    @Test
    public void externalEntityIsNotRead() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        Document d = createSaxBuilder().build(
                new StringReader(SampleXmlDocuments.externalResourceEntity(detector.getUrl())));

        assertFalse(detector.wasAttempted());
    }

    @Test
    public void dtdUriPointsToUrl() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        String s = SampleXmlDocuments.externalUrlDtd(detector.getUrl());

        Document d = createSaxBuilder().build(new StringReader(s));
        assertEquals("root", d.getRootElement().getName());
        assertEquals(0, d.getRootElement().getContent().size());

        assertFalse("I don't want to see HTTP connection attempts", detector.wasAttempted());
    }
}
