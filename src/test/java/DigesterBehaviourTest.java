import org.apache.commons.digester.Digester;
import org.apache.commons.digester.ExtendedBaseRules;
import org.junit.Test;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class DigesterBehaviourTest
{
    private Digester newSecureDigester() throws SAXException, ParserConfigurationException
    {
        final XMLReader xmlReader = SaxParserBehaviourTest.createXmlReader();
        final Digester digester = new Digester(xmlReader);
        digester.setEntityResolver(xmlReader.getEntityResolver()); //otherwise Digester will reset the resolver
        return digester;
    }

    public static class SomeTestClass
    {
        public String content;
        private static SomeTestClass parseResult;

        public void setX(String value)
        {
            content=value;
        }

        public void create(SomeTestClass o)
        {
            parseResult = o;
        }
    }

    @Test
    public void digesterParserIsBroken() throws IOException, SAXException
    {
        Digester digester = new Digester();
        runParser(digester, SampleXmlDocuments.externalResourceEntity());

        assertNotNull("Document not parsed", SomeTestClass.parseResult);
        assertFalse("An external entity has been resolved", SomeTestClass.parseResult.content.isEmpty());
    }

    @Test
    public void externalEntityIsNotRead() throws IOException, SAXException, ParserConfigurationException
    {
        Digester digester = newSecureDigester();
        runParser(digester, SampleXmlDocuments.externalResourceEntity());

        assertNotNull("Document not parsed", SomeTestClass.parseResult);
        assertEquals("An external entity has been resolved", "", SomeTestClass.parseResult.content);
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        Digester digester = newSecureDigester();

        runParser(digester, SampleXmlDocuments.AMPERSAND_DOCUMENT);
        assertEquals("&", SomeTestClass.parseResult.content);
    }

    @Test(expected = SAXParseException.class, timeout = 1000)
    public void parseBillionLaughsDoesNotExhaustMemory() throws Exception
    {
        Digester digester = newSecureDigester();
        runParser(digester, SampleXmlDocuments.BILLION_LAUGHS);
    }

    private void runParser(final Digester digester, final String toParse) throws IOException, SAXException
    {
        digester.setRules(new ExtendedBaseRules());

        digester.addObjectCreate("x", SomeTestClass.class);
        digester.addSetProperties("x");
        digester.addBeanPropertySetter("x");
        digester.addSetRoot("x", "create");

        digester.parse(new StringReader(toParse));
    }
}
