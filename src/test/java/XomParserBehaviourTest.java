import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;

import com.atlassian.security.xml.UntrustedXmlParserFactory;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.ParsingException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class XomParserBehaviourTest
{
    Builder newBuilder() throws ParserConfigurationException, SAXException
    {
        return new Builder(UntrustedXmlParserFactory.newXmlReader());
    }

    @Test
    public void testXomParserIsBroken() throws IOException, ParsingException
    {
        final InputStream in = this.getClass().getResourceAsStream("/evil.xml");
        assertNotNull("Could not load /evil.xml as stream.", in);
        Builder builder = new Builder();
        Document doc = builder.build(in);
        assertEquals("Did not load SYSTEM entity containing TOP SECRET", "TOP SECRET", doc.getValue().trim());
    }

    @Test
    public void testXomParserIsSafe() throws Exception
    {
        final InputStream in = this.getClass().getResourceAsStream("/evil.xml");
        assertNotNull("Could not load /evil.xml as stream.", in);

        Builder builder = newBuilder();
        Document doc = builder.build(in);
        assertThat(doc.getValue(), CoreMatchers.not(JUnitMatchers.containsString("TOP SECRET")));
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        Document d = newBuilder().build(new StringReader(SampleXmlDocuments.AMPERSAND_DOCUMENT));
        assertEquals("&", d.getRootElement().getValue());
    }

    @Test(expected = ParsingException.class, timeout=1000)
    public void parseBillionLaughsDoesNotExhaustMemory() throws Exception
    {
        newBuilder().build(new StringReader(SampleXmlDocuments.BILLION_LAUGHS));
    }

    @Test
    public void externalEntityIsNotIncludedInResults() throws Exception
    {
        Document d = newBuilder().build(new StringReader(SampleXmlDocuments.externalResourceEntity()));

        assertEquals("", d.getRootElement().getValue());
    }

    @Test
    public void externalEntityIsNotRead() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        Document d = newBuilder().build(
                new StringReader(SampleXmlDocuments.externalResourceEntity(detector.getUrl())));

        assertFalse(detector.wasAttempted());
    }

    @Test
    public void dtdUriPointsToUrl() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        String s = SampleXmlDocuments.externalUrlDtd(detector.getUrl());

        Document d = newBuilder().build(new StringReader(s));
        assertEquals("root", d.getRootElement().getLocalName());
        assertEquals(0, d.getRootElement().getChildCount());

        assertFalse("I don't want to see HTTP connection attempts", detector.wasAttempted());
    }
}
