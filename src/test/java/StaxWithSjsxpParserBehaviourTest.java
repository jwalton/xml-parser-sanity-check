import org.junit.Assume;
import org.junit.Before;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.StringReader;


/**
 * This test will only run with JDK 1.6.
 */
public class StaxWithSjsxpParserBehaviourTest extends StaxParserBehaviourTest
{
    @Override
    XMLStreamReader createReader(String input) throws Exception
    {
        XMLInputFactory xmlInputFactory = (XMLInputFactory) Class.forName("com.sun.xml.internal.stream.XMLInputFactoryImpl").newInstance();

        xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);

        return xmlInputFactory.createXMLStreamReader(new StringReader(input));
    }

    @Before
    public void skipTestIfOnJdk5()
    {
        try
        {
            Class.forName("com.sun.xml.internal.stream.XMLInputFactoryImpl");
        }
        catch (ClassNotFoundException e)
        {
            Assume.assumeNoException(e);
        }
    }
}
