#!/bin/sh

# Try to run the tests with different XML parsers

set -e

echo '||Group||Artifact||Version||Result||'

{
# Dummy - fall back to JDK
echo 'junit junit 4.8.2'

# Xerces
for v in 2.0.0 2.0.2 2.2.1 2.3.0 2.4.0 2.5.0 2.6.0 2.6.1 2.6.2 2.7.1 2.8.0 2.8.1 2.9.1 2.10.0; do
  echo xerces xercesImpl $v
done

# Aelfred
echo 'aelfred aelfred 1.2'

} | while read g a v; do

  # Makes plenty of assumptions about the pom
  cat pom.xml | patch -i patchable-pom.diff -f -r- -o- |
   sed -e "s,GROUPID,$g," -e "s,ARTIFACTID,$a," -e "s,VERSION,$v," >pom.xml.modified

  echo -n "|$g|$a|$v|"

  if mvn -f pom.xml.modified clean verify >&2; then
    echo '(/)|'
  else
    echo '(x)|'
  fi
done
