Different settings break different things. These are simple unit tests
to demonstrate the problems and allow different properties and features
to be tried.

Edit the 'create' methods and see what passes and fails.

Try running 'mvn test' with different JVMs and different (or no)
versions of Xerces. Are there any other parsers to consider?

I expect this to become an UntrustedXmlParserFactory class with factory
methods to create parser instances; for now, copy and paste.
